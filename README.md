# Basics of CT Reconstruction: Theory to Implementation

The contents of this repo were put together for the Signal and Image Processing in Biomedicine (PBM M209) course in Fall 2022.

The key topics of the lecture are 
- To introduce the fundamentals of image acquisition in CT
- State and prove the Fourier Slice Theorem (FST)
- Use FST to derive filtered backprojection reconstruction equation
- Motivate and derive band-limited filtering
- Use of window functions (e.g., Hanning) to illustrate controlling frequency response in our reconstruction
- Use LSF/MTF to quantify spatial resolution properties of imaging system

Some of the MATLAB code in this repository depends on the [Image Processing Toolbox](https://www.mathworks.com/products/image.html), which you will need to install prior to running any of the scripts/functions. Please file an issue if you are unable to obtain the Image Processing toolbox, and I can work to remove the dependency.

## Key Demos

**Backprojection Demo** 

`backprojection_demo.m`

Here we illustrate the basics of backprojection, including backprojection without filtration, "ideal" (instead of band-limited) ramp filtration, and band-limited ramp filtration.

**Windowing Demo**

`windowing_demo.m`

Here we illustrate the use of a Hanning window to control spatial resolution properties of the reconstructed image.

**MTF Demo**

`mtf_demo.m`

Here we illustrate a *very* simple example of computing the modulation transfer function from a simulated line phantom, and the different results we obtain from the windows used in the windowing demo.

## Lecture Notes

*Lecture notes will be uploaded soon.  I will scan them in when I'm in the office next.*

## Questions/Comments

Feel free to email me at jmhoffman [at] mednet.ucla.edu.  I can also be reached on the UC Tech slack system.  You can also file issues in this repo, however you will need to make a Gitlab account to do so (if you don't already have one.)