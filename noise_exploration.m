%% Illustrate Noise Injection
% The goal of this demo is to illustrate some VERY simple noise injection 
% our raw data anwhcih d the effect that that can have on our reconstruction.
% This is a WIP.

%% Generate our sinogram data
dim = 256;
n_proj = 1152/2;
th = [0:180/n_proj:180]; th = th(1:end-1);
ph = phantom(256);
[sino_org, R] = radon(ph,th);

stack = zeros(256,256,50);
dt = 1;

idx = 1;
for n_photons=100:-2:1
    sino = add_noise(sino_org, n_photons);
    inf_mask = isinf(sino);
    non_inf = sino(~inf_mask);
    max_val = max( non_inf(:) );
    sino(isinf(sino)) = 2*max_val;

    filt_medium = generate_filter(R, dt, 0.5, .5);
    sino_filt = zeros(size(sino));
    for i = 1:size(sino,2)
        sino_filt(:,i) = conv(sino(:,i), filt_medium, 'same');
    end
    
    stack(:,:,idx) = backproject(sino_filt, R, th, 256);
    idx = idx + 1;
    
end

viewer(stack)

%% Cooking show hanning
function f=generate_filter(k,ds,c,a)
% Simple function for creating filters
%
% Inputs:
%    k - list of indices (like [-200 -199 -198 ... 198 199])
%    ds - detector spacing (r_f*sin(fan_angle_increment/2))
%    c - [0,1]
%    a - [1, 0.5 0.54]
    
    f=(c^2/(2*ds))*(a*r(c*pi*k)+((1-a)/2)*r(pi*c*k+pi)+((1-a)/2)*r(pi*c*k-pi));
end

function vec=r(t)
vec=(sin(t)./t)+(cos(t)-1)./(t.^2);
vec(t==0)=0.5;
end