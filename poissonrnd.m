function counts = poissonrnd(lambda)

k = 0;

completed = false(size(lambda));
counts = zeros(size(lambda));
prob = zeros(size(lambda));

total_nums = length(lambda(:));
while true 
    fprintf(1, "%d\n", k);

    prob = prob + lambda.^k .* exp(-lambda)/factorial(k);
    %prob
    less_than = (rand(size(prob)) < prob);
    
    updated = less_than & ~completed;
    
    counts(updated) = k;
    completed = completed | updated;
    %completed
    
    k = k + 1;
    
    if sum(completed(:)) == total_nums
        break 
    end

end


end
