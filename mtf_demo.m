%% Generate our sinogram data
dim = 256;
n_proj = 1152/2;
th = [0:180/n_proj:180]; th = th(1:end-1);
ph = line_phantom(256);
[sino_org, R] = radon(ph,th);
sino = sino_org;

%% Apply our Hanning window to the band-limited filter
dt = 1; % detector size
filt_smooth = generate_filter(R, dt, 0.25, .5);
filt_medium = generate_filter(R, dt, 0.5, .5);
filt_ramp = generate_filter(R, dt, 1.0, 1.0);

%% Reconstructions with each
% Note: I think there might be a 1/2 pixel shift somewhere in my
% reconstruction code that causes the line phantom to be split over 2
% pixels rather than just 1.  Keep in mind that all of this is just for
% illustration and we would need to chase down that potential bug in a more 
% mature version of the MTF calculation.

sino_smooth = filter_sinogram(sino, filt_smooth);
recon_smooth = backproject(sino_smooth, R, th, 256);

sino_medium = filter_sinogram(sino, filt_medium);
recon_medium = backproject(sino_medium, R, th, 256);

sino_ramp = filter_sinogram(sino, filt_ramp);
recon_ramp = backproject(sino_ramp, R, th, 256);

figure;
subplot(1, 4, 1);
imshow(ph, []);
title('Phantom');

h = subplot(1, 4, 2);
imshow(flipud(recon_smooth), []);
title('Smooth');

subplot(1, 4, 3);
imshow(flipud(recon_medium), []);
title('Medium');

subplot(1, 4, 4);
imshow(flipud(recon_ramp), []);
title('Sharp (ramp)');

%% Grab our LSFs

lsf_smooth = recon_smooth(:,128);
lsf_medium = recon_medium(:,128);
lsf_ramp = recon_ramp(:, 128);

figure
subplot(1,2,1);
title('LSF')
hold on;
plot(lsf_smooth);
plot(lsf_medium);
plot(lsf_ramp);
legend('smooth', 'medium', 'ramp');

subplot(1,2,2);
title('MTF');
hold on

mtf_smooth = abs(fft(lsf_smooth));
mtf_smooth = mtf_smooth(1:dim/2)/mtf_smooth(1); % Normalize to zero bin

mtf_medium = abs(fft(lsf_medium));
mtf_medium = mtf_medium(1:dim/2)/mtf_medium(1); % Normalize to zero bin

mtf_ramp = abs(fft(lsf_ramp));
mtf_ramp = mtf_ramp(1:dim/2)/mtf_ramp(1); % Normalize to zero bin

plot(mtf_smooth);
plot(mtf_medium);
plot(mtf_ramp);
legend('smooth', 'medium', 'ramp');

%% Cooking show hanning
function f=generate_filter(k,ds,c,a)
% Simple function for creating filters
%
% Inputs:
%    k - list of indices (like [-200 -199 -198 ... 198 199])
%    ds - detector spacing (r_f*sin(fan_angle_increment/2))
%    c - [0,1]
%    a - [1, 0.5 0.54]
    
    f=(c^2/(2*ds))*(a*r(c*pi*k)+((1-a)/2)*r(pi*c*k+pi)+((1-a)/2)*r(pi*c*k-pi));
end

function vec=r(t)
vec=(sin(t)./t)+(cos(t)-1)./(t.^2);
vec(t==0)=0.5;
end

function sino_filt = filter_sinogram(sino, filt) 
    sino_filt = zeros(size(sino));
    for i = 1:size(sino,2)
        curr_proj = sino(:,i);
        sino_filt(:,i) = conv(curr_proj, filt, 'same');
    end    
end

%% Cooking show phantom

function ph = line_phantom(size)
ph = zeros(size);

for i = 1:size/2
    ph(size/2, i + size/4) = 1;
end

end