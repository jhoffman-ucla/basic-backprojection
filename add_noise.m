function noisy_sino = add_noise(sino, I0)

% Convert attenuations into counts
m = max(sino(:));
count_ratios = exp(-sino/m);
N = I0 * count_ratios;

% Add noise to count data
N = poissonrnd(N);

% Convert count data back to attenuation values
noisy_sino = -m * log(N/I0);

end