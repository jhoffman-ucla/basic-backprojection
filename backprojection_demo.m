%% Generate our sinogram data
dim = 256;
n_proj = 1152/2;
th = [0:180/n_proj:180]; th = th(1:end-1);
ph = phantom(dim);
[sino_org, R] = radon(ph,th);
sino = sino_org;


%% Naive Backprojection
% Note: I don't actually recommend running this version of our
% backprojection. Too slow.
%recon_img_naive = backproject_naive(sino_org, R, th, 256);

%% Faster backprojection
recon_img_naive = backproject(sino_org, R, th, 256);

%% Display our image
plot_recon(ph, sino, recon_img_naive);

%% Filter our sinogram w/ naive ramp filter
filt = fftshift(abs(R));
sino_filt = zeros(size(sino));
for i = 1:size(sino, 2) 
    sino_filt(:,i) = real(ifft(fft(sino(:,i)).*filt));
end

%% Backproject the *filtered* projections
recon_img_filt = backproject(sino_filt, R, th, 256);

%% Display
plot_recon(ph, sino_filt, recon_img_filt);

%% Generate a band-limited ramp filter

dt = 1; % detector "size"
filt_bl = zeros(size(R));

for i = 1:length(filt_bl) 
    
    t = R(i);
    if t == 0
        filt_bl(i) = 1/(4*dt*dt);
    elseif mod(t,2) == 1
        filt_bl(i) = -1/(t*pi*dt * t*pi*dt);
    else
        filt_bl(i) = 0;
    end
    
end

selector = R>=-10 & R<=10;

figure;
title("Band-limited Ramp filter (spatial domain)")
plot(R(selector),filt_bl(selector))

filt_bl_fourier = (length(R) - 1) * fftshift(real(fft(fftshift(filt_bl(1:end-1)))));

% Plot the band-limited filter vs the ideal ramp filter
figure;
title('Ramp Filters (Fourier domain)')
plot(R(selector), filt_bl_fourier(selector)); hold on; plot(R(selector),abs(R(selector)))
legend('band-limited', 'ideal');

%% Generate our sinogram with the band-limited filter, using convolution
sino_filt_bl = zeros(size(sino_org));
for i = 1:size(sino_org,2)
    curr_proj = sino_org(:,i);
    sino_filt_bl(:,i) = conv(curr_proj, filt_bl, 'same');
end

%% Backproject the band-limited ramp filter
recon_img_filt_bl = backproject(sino_filt_bl, R, th, 256);

%% Display 
plot_recon(ph, sino_filt_bl, recon_img_filt_bl);
