function recon_img = backproject_naive(sino, R, th, fov)
% This version of our function should be easier to translate to another 
% programming language, however is not efficient in MATLAB. The worst
% offender here is the 'interp1' function, which we are using very
% inefficiently.
%
% Results/output are the same between backproject and backproject_naive

n_proj = size(sino,2);
    
x_dim = 128;
y_dim = 128;

dx = fov / x_dim; 
dy = fov / y_dim;

recon_img = zeros(y_dim, x_dim);

% Outer loop(s) iterate over all pixels
for ix = 1:x_dim
    fprintf(1, "column %d\n", ix);
    x = (ix - 1 - (x_dim-1)/2) * dx;
    for iy = 1:y_dim
        y = (iy - 1 - (y_dim-1)/2) * dy;

        % Inner loop calculates the current x,y voxel by looping over all
        % projections in our incoming sinogram
        voxel = 0;
        for itheta = 1:length(th)
            theta = deg2rad(th(itheta));
            t = x * cos(theta) + y * sin(theta);
            curr_proj = sino(:, itheta);
            voxel = voxel + interp1(R, curr_proj, t, 'linear');
        end
        
        recon_img(iy, ix) = voxel*pi/n_proj;
    end
end

end