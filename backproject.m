function recon_img = backproject(sino, R, th, fov)
% This version of backprojection is a much more MATLAB friendly
% implementation, but will be more challenging to translate to another
% programming language. We make heavy use of MATLAB's matrices and
% vectorization.
%
% Results/output are the same between backproject and backproject_naive

n_proj = size(sino,2);
    
x_dim = 256;
y_dim = 256;

dx = fov / x_dim; 
dy = fov / y_dim;

x_cent = (x_dim - 1)/2;
y_cent = (y_dim - 1)/2;

recon_img = zeros(y_dim, x_dim);

% precompute our interpolation points
x = ([0:x_dim-1] - x_cent)*dx;
y = ([0:y_dim-1] - y_cent)*dy;
[X, Y] = meshgrid(x,y);

for itheta = 1:length(th)
    curr_proj = sino(:,itheta);
    theta = deg2rad(th(itheta));
    t = X * cos(theta) + Y * sin(theta);
    v_update = interp1(R, curr_proj, t(:), 'linear');
    v_update = reshape(v_update, size(X));
    recon_img = recon_img + v_update;
end

recon_img = recon_img * pi / n_proj;

end

