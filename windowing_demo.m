%% Generate our sinogram data
dim = 256;
n_proj = 1152/2;
th = [0:180/n_proj:180]; th = th(1:end-1);
ph = phantom(256);
[sino_org, R] = radon(ph,th);
sino = sino_org;

%% Apply our Hanning window to the band-limited filter
dt = 1; % detector size
filt_smooth = generate_filter(R, dt, 0.25, .5);
filt_medium = generate_filter(R, dt, 0.5, .5);
filt_ramp = generate_filter(R, dt, 1.0, 1.0);

%% Reconstructions with each

sino_smooth = filter_sinogram(sino, filt_smooth);
recon_smooth = backproject(sino_smooth, R, th, 256);

sino_medium = filter_sinogram(sino, filt_medium);
recon_medium = backproject(sino_medium, R, th, 256);

sino_ramp = filter_sinogram(sino, filt_ramp);
recon_ramp = backproject(sino_ramp, R, th, 256);

figure;
subplot(1, 4, 1);
imshow(ph, []);
title('Phantom');

subplot(1, 4, 2);
imshow(flipud(recon_smooth), []);
title('Smooth');

subplot(1, 4, 3);
imshow(flipud(recon_medium), []);
title('Medium');

subplot(1, 4, 4);
imshow(flipud(recon_ramp), []);
title('Sharp (ramp)');

%% Cooking show hanning
function f=generate_filter(k,ds,c,a)
% Simple function for creating filters
%
% Inputs:
%    k - list of indices (like [-200 -199 -198 ... 198 199])
%    ds - detector spacing (r_f*sin(fan_angle_increment/2))
%    c - [0,1]
%    a - [1, 0.5 0.54]
    
    f=(c^2/(2*ds))*(a*r(c*pi*k)+((1-a)/2)*r(pi*c*k+pi)+((1-a)/2)*r(pi*c*k-pi));
end

function vec=r(t)
vec=(sin(t)./t)+(cos(t)-1)./(t.^2);
vec(t==0)=0.5;
end

function sino_filt = filter_sinogram(sino, filt) 
    sino_filt = zeros(size(sino));
    for i = 1:size(sino,2)
        curr_proj = sino(:,i);
        sino_filt(:,i) = conv(curr_proj, filt, 'same');
    end    
end