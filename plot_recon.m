function plot_recon(phantom, sinogram, recon)

figure
subplot(1,3,1);
imshow(phantom,[0 1]);
subplot(1,3,2);
imshow(sinogram,[]);
subplot(1,3,3);
imshow(flipud(recon),[]);

end

